package com.example.root.appvolley;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity{
    private TextView txtvJson;
    private EditText editNombre;
    private EditText editDesc;
    private Button btnEnviarPOST;
    private Button btnEnviarGET;
    private ArrayList<Producto> arrayProduc = new ArrayList<Producto>();

    private static final String URLApi = "http://192.168.0.194/volley/productos.php";
    //private static final String URLApi = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtvJson = (TextView) findViewById(R.id.txtv_json);
        editNombre = (EditText) findViewById(R.id.editText);
        editDesc = (EditText) findViewById(R.id.editText2);

        btnEnviarPOST = (Button) findViewById(R.id.button_put);
        btnEnviarGET = (Button) findViewById(R.id.button_get);

        btnEnviarGET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertar(editNombre.getText().toString(), editDesc.getText().toString());
            }
        });
        btnEnviarPOST.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getProducts();
            }
        });

    }

//    @Override
//    public void onClick(View v){
//        switch(v.getId()){
//            case btnEnviarGET:
//        }
//
//    }
//
//    public int getId(View v){
//        return findViewById(R.id.button_put);
//    }

    public void insertar(final String nombre, final String descripcion){
        // listener callback  y error
        StringRequest peticion = new StringRequest(Request.Method.POST, URLApi, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    txtvJson.setText(jsonObject.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "error"+error.toString() , Toast.LENGTH_SHORT).show();
                System.out.println("error"+error);
                Log.d("error", "onErrorResponse: "+error);
            }

        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parametros = new HashMap<String, String>();
                parametros.put( "metodo", "insertar");
                parametros.put( "nombre", nombre);
                parametros.put( "descripcion", descripcion);

                return parametros;
            }
        };
        MySingleton.getInstance(this).addToRequestQueue(peticion);
    }


    //FIXME get
    public void getProducts(){
        // listener callback  y error
        StringRequest peticionGet = new StringRequest(Request.Method.POST, URLApi, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    int bandera = jsonObject.getInt("bandera");
                    if (bandera == 1 ){
                        Toast.makeText(MainActivity.this, "Hay objetos ", Toast.LENGTH_SHORT).show();
                        //String  mensaje = jsonObject.getString("vista_productos");
                        JSONArray arrayProductos = jsonObject.getJSONArray("vista_productos");
                        for(int j = 0; j<arrayProductos.length(); j++){
                            JSONObject objeto =  arrayProductos.getJSONObject(j);
                            int idAux = objeto.getInt("id");
                            String nombre = objeto.getString("nombre");
                            String descripcion = objeto.getString("descripcion");

                            Producto producto = new Producto(idAux,nombre,descripcion);

                            //TODO hacer funcion  enviar json, regresa objeto
                            arrayProduc.add(producto);
                        }

                        for(int j = 0; j<arrayProduc.size(); j++){
                            txtvJson.append(arrayProduc.get(j).toString());
                            txtvJson.append("\n");
                        }

                    }else{
                        Toast.makeText(MainActivity.this, "No hay nada :c  ", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "error"+error.toString() , Toast.LENGTH_SHORT).show();
            }

        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> parametros = new HashMap<String, String>();
                parametros.put( "metodo", "buscar");
                return parametros;
            }
        };
        MySingleton.getInstance(this).addToRequestQueue(peticionGet);
    }

}
